// Gulpfile.js configuration

// gulp del
// https://www.npmjs.com/package/del
// npm install --save del

// gulp-newer
// https://www.npmjs.com/package/gulp-newer
// npm install gulp-newer --save-dev

// gulp-imagemin
// https://www.npmjs.com/package/gulp-imagemin
// npm install --save-dev gulp-imagemin

// gulp-sass
// https://www.npmjs.com/package/gulp-sass
// npm install gulp-sass --save-dev

// gulp-connect
// https://github.com/AveVlad/gulp-connect/
// npm install --save-dev gulp-connect


// include gulp
const	gulp = require('gulp'),

// modules
			del = require('del'),
			newer = require('gulp-newer'),
			imagemin = require('gulp-imagemin'),
            sass = require('gulp-sass'),
            connect = require('gulp-connect');

// folders
const	folder = {
				src: 'src/',
				build: 'build/'
			};

// variables
const	ignore = '!build/.gitkeep',
            favicon = 'favicon.webp',
            htaccess = '.htaccess',
			outBuild = folder.build;

// TASKS

// Clean the build folder
	gulp.task('del', function () {
	  del([
			outBuild + '/*', ignore
	  ], {dot: true});
    });
    
// Update Favicon on build folder
	gulp.task('favicon', function () {
		return gulp.src([
            folder.src + favicon, 
            folder.src + htaccess
        ])
		.pipe(newer(outBuild))
		.pipe(gulp.dest(outBuild));
	});
    
// copy all static HTML to build folder
    gulp.task('html', function() {
        return gulp.src(folder.src + 'views/*.html')
        .pipe(newer(outBuild))
        .pipe(gulp.dest(outBuild))
        .pipe(connect.reload());
    });

// copy all static CSS to build folder
    gulp.task('css', function() {
        var out = outBuild + 'assets/css/';
        return gulp.src(folder.src + 'assets/css/**/*')
        .pipe(newer(out))
        .pipe(gulp.dest(out));
    });

// copy all static JS to build folder
    gulp.task('js', function() {
        var out = outBuild + 'assets/js/';
        return gulp.src(folder.src + 'assets/js/**/*')
        .pipe(newer(out))
        .pipe(gulp.dest(out))
        .pipe(connect.reload());
    });

// image processing optimize
    gulp.task('images', function() {
        var out = outBuild + 'assets/img/';
        return gulp.src(folder.src + 'assets/img/**/*')
        .pipe(newer(out))
        .pipe(imagemin())
        .pipe(gulp.dest(out));
    });

// sass to build/assetes/css
    gulp.task('sass', function () {
    return gulp.src([folder.src + '/sass/*', '!src/sass/_*/'])
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(outBuild + '/assets/css'))
        .pipe(connect.reload());
    });
// Set live server http://localhost:8001/
    gulp.task('localhost', function() {
        connect.server({
            root: outBuild,
            port: 8001,
            livereload: true
        });
    });
// Live reload server - Watching changes
    gulp.task('watch', function () {
        gulp.watch([folder.src + 'views/*.html'], ['html']);
        gulp.watch([folder.src + '/sass/*'], ['sass']);
        gulp.watch([folder.src + 'assets/css/**/*'], ['css']);
        gulp.watch([folder.src + 'assets/js/**/*'], ['js']);
        gulp.watch([folder.src + 'assets/img/**/*'], ['images']);
    });

// Run clear/del task - it's clearing build directory
  gulp.task('clear', ['del']);
  
// Run all Tasks
  gulp.task('tasks', ['html', 'css', 'js', 'favicon', 'images', 'sass', 'localhost', 'watch']);