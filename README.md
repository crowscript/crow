# AMZ
---
AMZ.one - The only tool you need to improve your Amazon sales

## Table of Contents
---
- [About the Project](#about-the-project)
- [Status](#status)
- [Screenshots](#screenshots)
- [Live](#live)
  - [Current](#current)
  - [Demo](#demo)
- [Directory Structure](#directory-structure)
- [Getting Started](#getting-started)
- [Built With](#built-with)
  - [Requirements](#requirements)
  - [Dependencies](#dependencies)
- [Release Process](#release-process)
  - [Changelog](#changelog)
- [Authors](#authors)

## About the Project
---
Redesign of website [AMZ.one](https://www.amz.one/)

## Status
---
![Node](https://img.shields.io/badge/node-v8.11.2-%23026e00.svg) ![Gulp](https://img.shields.io/badge/gulp-v3.9.1-%23cf4647.svg) ![npm](https://img.shields.io/badge/npm-v5.6.0-%23833.svg) ![Bootstrap](https://img.shields.io/badge/bootstrap-v3.3.6-%237952b3.svg) ![sass](https://img.shields.io/badge/css-sass-%23c6538c.svg)

## Screenshots
---
![Logo](https://www.amz.one/images/logo-bst-new.png)
![Screen](https://www.amz.one/images/500x281xNegativeReviews.jpg.pagespeed.ic.R10dLl1lJT.webp)

## Live
---
### Current
[www.amz.one/](https://www.amz.one/)
### Demo
[localhost-amz](http://localhost:8001/)

## Directory Structure
---
```
┌── build
│   └──  .gitkeep
├── src
│   ├── assets
│   │   ├── css
│   │   │   ├── bootstrap
│   │   │   │   ├── bootstrap.min.css
│   │   │   │   └── bootstrap-theme.min.css
│   │   │   ├── fonts
│   │   │   │   ├── FontAwesome
│   │   │   │   ├── fontawesome-webfont
│   │   │   │   └── ionicons
│   │   │   ├── font-awesome.min.css
│   │   │   ├── ionicons.min.css
│   │   │   ├── magnific-popup.css
│   │   │   ├── modaal.min.css
│   │   │   └── owl.carousel.css
│   │   ├── img
│   │   │   └── ...
│   │   └── js
│   │       ├── bootstrap
│   │       │   └── bootstrap.min.js
│   │       ├── jquery.appear.js
│   │       ├── jquery.countTo.js
│   │       ├── jquery.magnific-popup.min.js
│   │       ├── jquery.min.js
│   │       ├── jquery.scrollTo.min.js
│   │       ├── jquery.scrolly.js
│   │       ├── main.js
│   │       ├── modaal.min.js
│   │       ├── owl.carousel.min.js
│   │       ├── pace.min.js
│   │       ├── plugins-scroll.js
│   │       └── smooth.scroll.min.js
│   ├── sass
│   │   ├── _animations.scss
│   │   ├── _core.scss
│   │   ├── _magnificpopup.scss
│   │   ├── _main.scss
│   │   ├── _mixins.scss
│   │   ├── _navigation.scss
│   │   ├── _owl.scss
│   │   ├── _variables.scss
│   │   └── style.scss
│   └── views
│       ├── .gitkeep
│       └── index.html
├── .gitignore
├── gulpfile.js
├── package.json
├── package-lock.json
└── README.md
```

## Getting Started
---
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

1. If needed, [install](http://blog.nodeknockout.com/post/65463770933/how-to-install-node-js-and-npm) `node` and `npm` (Node Package Manager).
- If needed, install `gulp` with `npm install gulp-cli -g` for globally. 
  - Install `gulp` locally `npm install gulp --save-dev`
- Install Gulp packages trough terminal
  - Package: [gulp del](https://www.npmjs.com/package/del) `npm install --save del`
  - Package: [gulp-newer](https://www.npmjs.com/package/gulp-newer) `npm install gulp-newer --save-dev`
  - Package: [gulp-imagemin](https://www.npmjs.com/package/gulp-imagemin) `npm install --save-dev gulp-imagemin`
  - Package: [gulp-sass](https://www.npmjs.com/package/gulp-sass) `npm install gulp-sass --save-dev`
  - Package: [gulp-connect](https://github.com/AveVlad/gulp-connect/) `npm install --save-dev gulp-connect`
- Clone this repo with `git clone https://crowscript@bitbucket.org/crowscript/crow.git`.
- In terminal, enter `gulp tasks` to run all Gulp tasks.
- Open `http://localhost:8001`.
- Edit your code inside of the `src` folder.
- Your complied files will be created and updated in `build/`. NEVER edit files within the `build/` folder, as it gets deleted frequently.
- Keep `gulp` running while you're making changes. When you want to stop the gulp task, hit `ctrl + c`.


## Built With
---
* [Bootstrap](https://getbootstrap.com/docs/3.3/getting-started/) - The web framework used
* [Gulp](https://gulpjs.com/) - Dependency Management
* [SASS](https://sass-lang.com/)

### Requirements
- Node/NPM
- Gulp
### Dependencies
```
    "gulp": "^3.9.1",
    "gulp-connect": "^5.5.0",
    "gulp-imagemin": "^4.1.0",
    "gulp-newer": "^1.4.0",
    "gulp-sass": "^4.0.1"
    "del": "^3.0.0"
```

## Release Process
---
### Changelog
**v1.0.0**
- Implemented form in the footer section
- Changed Hero images
- Redesign of the pricing table

## Authors
---
* **Stanislav Vranic** - *Front End Developer* - [@crowscript](http://crowscript.com)

**[Back to top](#table-of-contents)**
